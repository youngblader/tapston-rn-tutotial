import React from 'react';
import { AppRegistry, Text, TextInput } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';

import RNStyles from '@tapston/react-native-styles';

import App from './source/app';
import { name as appName } from './app.json';

Text.defaultProps = {
  ...(Text.defaultProps || {}),
  allowFontScaling: false,
};
TextInput.defaultProps = {
  ...(TextInput.defaultProps || {}),
  allowFontScaling: false,
};

RNStyles.init({
  designWidth: 390,
  designHeight: 844,
});

const AppContainer = () => {
  return (
    <NavigationContainer>
      <App />
    </NavigationContainer>
  );
};
AppRegistry.registerComponent(appName, () => AppContainer);
