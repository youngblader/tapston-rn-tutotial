### Status Bar

Для того чтобы StatusBar работал адекватно и менял состояние в зависимости от экрана, рекомендуется
использовать следующий компонент отдельно на каждом экране.

```js
import React from 'react';
import { StatusBar } from 'react-native';
import { useIsFocused } from '@react-navigation/native';

const FocusAwareStatusBar = (props) => {
  const isFocused = useIsFocused();
  return isFocused ? <StatusBar {...props} /> : null;
};

export default FocusAwareStatusBar;
```


Компонент принимает параметры компонента StatusBar из библиотеки `react-native`, к примеру:
`backgroundColor` и `barStyle`.

```js
<FocusAwareStatusBar backgroundColor="#fff" barStyle="darkContent" />
```
