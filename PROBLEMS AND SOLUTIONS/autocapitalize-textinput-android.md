### Auto Capitalize TextInput

На Android по состоянию на версию RN 0.63.3 существует не решенный баг с
дублированием символов в TextInput при попытке сделать autoCapitalize любым
из способов.

Решение проблемы:

#### Проверено, что не работает на старом xiaomi с версией Android 7


```js

<TextInput
  onChange={(text) => {
    setValue(text.toUpperCase());
  }}
  secureTextEntry={Platform.OS !== 'ios'}
  keyboardType={Platform.OS === 'ios' ? null : 'visible-password'}
/>

```
