### Геолокация в foreground

### Геолокация в Background

В RN существует два самых известных модуля для работы с геолокацией в background:

Бесплатный:
https://github.com/mauron85/react-native-background-geolocation

Платный:
https://transistorsoft.github.io/react-native-background-geolocation/interfaces/_react_native_background_geolocation_.config.html

Модуль от mauron85 по сути является ранней копией модуля от transistorsoft, уже не поддерживается и имеет ряд проблем. При этом, он полностью бесплатен и при необходимости для определения геолокации пользователя в фоне можно использовать именно его.
