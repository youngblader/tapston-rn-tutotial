### iOS эмулятор

#### Запуск не стандартного эмулятора
Название эмулятора как указано в xCode, либо как было установлено относительно UUID (описано ниже)

`npx react-native run-ios --simulator="iPhone SE (2nd generation)"`

#### Управление эмулятором через консоль

- Cписок всех iOS девайсов - `xcrun simctl list devices`
- Задать название девайсу по UUID - `xcrun simctl MyDevice CB87B315-F01A-41AA-9C85-6FE24E5A66B9`
- Открытие ссылки на эмуляторе - `xcrun simctl openurl MyDevice "https://www.google.com/"`
- Информация о приложении по bundleId - `xcrun simctl appinfo MyDevice com.tapston.application`

#### Управление статус-баром эмулятора через консоль
- `xcrun simctl status_bar MyDevice override ФЛАГИ`
##### Возможные флаги:
- --time 10:30
- --dataNetwork wifi
- --wifiMode active
- --wifiBars 2
- --cellularMode active
- --cellularBars 3
- --operatorName Tapston
- --batteryState charging
- --batteryLevel 75

---
Больше информации: https://habr.com/ru/post/506504/
