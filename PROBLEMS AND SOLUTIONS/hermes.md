### Hermes

### Из документации RN:

Hermes is an open-source JavaScript engine optimized for React Native. For many apps, enabling Hermes will result in improved start-up time, decreased memory usage, and smaller app size. At this time Hermes is an opt-in React Native feature, and this guide explains how to enable it.

### Документация:
https://hermesengine.dev/

### Исходный код:
https://github.com/facebook/hermes

### Установка hermes (по ссылке необходимо выбрать вашу версию RN):
https://reactnative.dev/docs/hermes

### Дополнительно:
- Поддерживается Android с версии RN 0.62
