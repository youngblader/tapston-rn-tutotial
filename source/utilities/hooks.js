import {useRef} from 'react';

/**
 * Lifecycle hook: componentWillMount
 * @param func - callback of the hook
 */
export const useComponentWillMount = (func) => {
  const willMount = useRef(true);

  if (willMount.current) {
    func();
  }

  willMount.current = false;
};
