## SwitchTabs

### Переключатели кнопок с плавающей анимацией 

#### Для работы анамации необходима библиотека animateLayout

```bash
$ npm i @tapston/react-native-animation
```
##### Or

```bash
$ yarn add @tapston/react-native-animation
```

## Использование

```javascript
const tabs = [
  {label: 'Заработать баллы', value: 'points', badge: false },
  {label: 'История', value: 'story', badge: true },
];

 <SwitchTabs
    options={tabs}
    onChange={handleChangeTab} // обработка нажатия на кнопку
    activeTab={activeTab}  // активная кнопка
/>
```