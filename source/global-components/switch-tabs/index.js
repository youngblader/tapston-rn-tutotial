import React, { useState } from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import RNStyles from '@tapston/react-native-styles';
import { animateLayout } from '@tapston/react-native-animation';

/**
 * SwitchTabs component
 * @prop {Array} options
 * @prop {Function} onChange
 * @prop {Number} activeTab
 * @prop {Object} containerStyles
 */
const SwitchTabs = ({
  options = [],
  onChange = () => {},
  activeTab = 0,
  containerStyles = {},
}) => {
  const [tabsWidth, setTabsWidth] = useState([]);
  const [position, setPosition] = useState(0);
  const moveActiveTab = index => {
    animateLayout();
    setPosition(index > 0 ? tabsWidth[index - 1] : 0);
  };

  const _onChange = index => {
    onChange(index);
    moveActiveTab(index);
  };
  const lessElementIndex = options.findIndex(el =>                 //находим таб с самым коротким текстом
    options.some(option => el.label.length < option.label.length),
  );
  return (
    <View style={[styles.container, containerStyles]}>
      {tabsWidth.length > 0 ? (
        <View
          style={[styles.activeTab, { width: tabsWidth[0], left: position }]}
        />
      ) : null}
      {options.map((tab, index) => (
        <View
          key={tab.key}
          style={[
            styles.tab,
            index === lessElementIndex ? { flex: 1 } : { maxWidth: '100%' },
          ]}
          onLayout={e => {
            e.persist();
            if (!tabsWidth[index]) {
              setTabsWidth(old => {
                let newOld = [...old];
                newOld[index] = e.nativeEvent.layout.width;
                return newOld;
              });
            }
          }}>
          <TouchableOpacity
            onPress={() => {
              _onChange(index);
            }}>
            <Text>
              {tab.label}
            </Text>
          </TouchableOpacity>
        </View>
      ))}
    </View>
  );
};

const styles = RNStyles.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 30,
    overflow: 'hidden',
    height: 48,
  },
  tab: {
    paddingHorizontal: 16,
    alignItems: 'center',
    justifyContent: 'center',
  },
  activeTab: {
    backgroundColor: 'blue',
    borderRadius: 30,
    position: 'absolute',
    top: 0,
    bottom: 0,
    zIndex: 0,
  },
});

export default SwitchTabs;
