import React from 'react';
import { View, Text } from 'react-native';

import RNStyles from '@tapston/react-native-styles';

const Profile = () => {
  const styles = getStyles();
  return (
    <View style={styles.container}>
      <Text>Profile</Text>
    </View>
  );
};

const getStyles = () =>
  RNStyles.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
  });

export default Profile;
