import React from 'react';
import { View, Text } from 'react-native';

import RNStyles from '@tapston/react-native-styles';
import { TouchableOpacity } from 'react-native-gesture-handler';

const Home = props => {
  const styles = getStyles();

  const handleNavigation = (screen, title) => {
    props.navigation.navigate(screen, { title });
  };

  return (
    <View style={styles.container}>
      <Text style={{ fontSize: 21 }}>Home</Text>
      <View>
        <TouchableOpacity
          onPress={() => handleNavigation('Cars', 'Cars Category')}>
          <Text style={{ fontSize: 15 }}>Cars Category</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => handleNavigation('Football', 'Football Category')}>
          <Text style={{ fontSize: 15 }}>Football Category</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => handleNavigation('Sports', 'Sports Category')}>
          <Text style={{ fontSize: 15 }}>Sports Category</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const getStyles = () =>
  RNStyles.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
  });

export default Home;
