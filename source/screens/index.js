import Home from './Home/Home';
import Profile from './Profile/Profile';
import Cart from './Cart/Cart';

export default {
  Home,
  Cart,
  Profile,
};
