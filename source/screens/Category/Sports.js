import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import RNStyles from '@tapston/react-native-styles';

const Sports = props => {
  const styles = getStyles();

  const handleGoBack = () => {
    props.navigation.goBack();
  };

  return (
    <View style={styles.container}>
      <Text>{props.route.params.title}</Text>
      <TouchableOpacity onPress={handleGoBack}>
        <Text>Back</Text>
      </TouchableOpacity>
    </View>
  );
};

const getStyles = () =>
  RNStyles.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
  });

export default Sports;
