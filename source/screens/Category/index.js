import Cars from './Cars';
import Football from './Football';
import Sports from './Sports';

export default {
  Cars,
  Football,
  Sports,
};
