import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import screens from '../screens';

const BottomBarNav = createBottomTabNavigator();
const BottomBarStack = () => {
  return (
    <BottomBarNav.Navigator initialRouteName="Home">
      <BottomBarNav.Screen
        name="Home"
        component={screens.Home}
        options={{ headerShown: false }}
      />
      <BottomBarNav.Screen
        name="Cart"
        component={screens.Cart}
        options={{ headerShown: false }}
      />
      <BottomBarNav.Screen
        name="Profile"
        component={screens.Profile}
        options={{ headerShown: false }}
      />
    </BottomBarNav.Navigator>
  );
};

export default BottomBarStack;
