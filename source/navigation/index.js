import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import screens from '../screens';
import category from '../screens/Category';

import BottomBarStack from './BottomBar';

const Stack = createStackNavigator();

const RootStack = () => {
  return (
    <Stack.Navigator initialRouteName="BottomBarStack">
      <Stack.Screen
        name="BottomBarStack"
        options={{
          headerShown: false,
        }}
        component={BottomBarStack}
      />
      <Stack.Screen
        name="Home"
        options={{
          headerShown: false,
        }}
        component={screens.Home}
      />
      <Stack.Screen
        name="Cart"
        options={{
          headerShown: false,
        }}
        component={screens.Cart}
      />
      <Stack.Screen
        name="Profile"
        options={{
          headerShown: false,
        }}
        component={screens.Profile}
      />
      <Stack.Screen
        name="Cars"
        options={{
          headerShown: false,
        }}
        component={category.Cars}
      />
      <Stack.Screen
        name="Football"
        options={{
          headerShown: false,
        }}
        component={category.Football}
      />
      <Stack.Screen
        name="Sports"
        options={{
          headerShown: false,
        }}
        component={category.Sports}
      />
    </Stack.Navigator>
  );
};

export default RootStack;
